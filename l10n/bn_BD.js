OC.L10N.register(
    "spreed",
    {
    "None" : "কোনটিই নয়",
    "User" : "User",
    "Everyone" : "সকলে",
    "Disabled" : "অকার্যকর",
    "Users" : "ব্যবহারকারী",
    "Favorite" : "প্রিয়জন",
    "Copy link" : "লিঙ্ক কপি করো",
    "Remove from favorites" : "Remove from favorites",
    "Add to favorites" : "Add to favorites",
    "Contacts" : "পরিচিতজন",
    "Loading" : "Loading",
    "Groups" : "গোষ্ঠীসমূহ",
    "Error" : "সমস্যা",
    "Back" : "পেছনে যাও",
    "Close" : "বন্ধ",
    "Reply" : "জবাব",
    "Today" : "আজ",
    "Yesterday" : "গতকাল",
    "guest" : "অতিথি",
    "Share link" : "লিংক ভাগাভাগি করেন",
    "Name" : "নাম",
    "Saved" : "সংরক্ষণ করা হলো",
    "Password" : "কূটশব্দ",
    "Cancel" : "বাতির",
    "The password is wrong. Try again." : "কুটশব্দটি ভুল। আবার চেষ্টা করুন।",
    "Share" : "ভাগাভাগি কর",
    "Change password" : "কূটশব্দ পরিবর্তন করুন",
    "Rename" : "পূনঃনামকরণ",
    "Send" : "পাঠাও",
    "Error while sharing" : "ভাগাভাগি করতে সমস্যা দেখা দিয়েছে  ",
    "Edit" : "সম্পাদনা",
    ", " : ", "
},
"nplurals=2; plural=(n != 1);");
